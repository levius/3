`timescale 1ns / 1ps

module main(
	input rst_i,
	input clk_i,
	input [7:0] a_in,
	input [7:0] b_in,
	output busy_o,
	output [15:0] y_bo
);

reg [7:0] a;
reg [7:0] b;
reg [15:0] y;
reg finish;
reg [3:0] state, state_next;

reg mult_rst;
reg [7:0] mult_a;
reg [7:0] mult_b;
wire [15:0] mult_out;
wire mult_busy;

reg [15:0] mult_res;
 
mul mul(
    .clk_i(clk_i),
    .rst_i(mult_rst),
    .a_bi(mult_a),
    .b_bi(mult_b),
    .busy_o(mult_busy),
    .y_bo(mult_out)
);

reg root_rst;
reg [7:0] root_x;
wire [7:0] root_out;
wire root_busy;

root root(
    .clk_i(clk_i),
    .rst_i(root_rst),
    .x_bi(root_x),    
    .busy_o(root_busy),
    .y_bo(root_out)
);

localparam IDLE = 4'b0000;
localparam STATE1 = 4'b0001;
localparam STATE2 = 4'b0010;
localparam STATE3 = 4'b0011;
localparam STATE4 = 4'b0100;
localparam STATE5 = 4'b0101;
localparam STATE6 = 4'b0110;
localparam STATE7 = 4'b0111;
localparam STATE8 = 4'b1000;
localparam STATE9 = 4'b1001;



assign busy_o = rst_i | |state;
//assign busy_o = busy;
assign y_bo = y;

always @(posedge clk_i) 
    if (rst_i) begin
        state <= STATE1;
    end else begin
        state <= state_next;
    end
    
always @* begin
    case(state)
        IDLE: state_next = IDLE;
        STATE1: state_next = STATE2;
        STATE2: state_next = (root_busy) ? STATE2 : STATE3;
        STATE3: state_next = (mult_busy) ? STATE3 : STATE4;
        STATE4: state_next = (finish) ? IDLE : STATE4;
    endcase
end

always @(posedge clk_i) begin
//    busy = rst_i | |state;
    if (rst_i) begin
        y <= 0;
        a <= a_in;
        b <= b_in;
        finish <= 0;
        mult_rst <= 0;
        root_rst <= 0;
    end else begin
    case (state)
        IDLE:
            begin
                finish <= 0;
            end
        STATE1:
            begin
                root_rst <= 1;
                root_x <= b;
            end
        STATE2:
            begin
                if (root_busy) begin
                    root_rst <= 0;
                end else begin
                    mult_rst <= 1;
                    mult_a <= root_out;
                    mult_b <= 2;
                end
            end
        STATE3:
            begin
                if (mult_busy) begin
                    mult_rst <= 0;
                end else begin
                    mult_res <= mult_out;
                    mult_rst <= 1;
                    mult_a <= 3;
                    mult_b <= a;
                end
            end
        STATE4:
            begin
                if (mult_busy) begin
                    mult_rst <= 0;
                end else begin
                    y = mult_out + mult_res;
                    finish = 1;
                end    
            end
    endcase
end end
endmodule

