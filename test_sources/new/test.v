
module test;

reg rst, clk;
wire clk_w, rst_w;
reg [7:0] a;
reg [7:0] b;
wire busy;
wire [15:0] y_bo;
reg [15:0] expected;

main main(
	.clk_i(clk),
	.rst_i(rst_w),
	.a_in(a),
	.b_in(b),
	.busy_o(busy),
	.y_bo(y_bo)
);

assign rst_w = rst;
assign clk_w = clk;

initial begin
	clk = 1;
	forever
		#10 clk = ~clk;
end

initial begin
	a <= 0;
	b <= 0;
	rst <= 1;
	$display("y = 3*a + 2*(b^0.333)");
end

always @(posedge clk) begin
	if (rst) begin
		rst <= 0;
		expected <= 0;
	end else begin 
	   if (b == 0) begin
	       expected <= 3 * a;
	   end else if (b < 8) begin
	       expected <= 2 + 3 * a;
	   end else if (b < 27) begin
	       expected <= 4 + 3 * a;
	   end else if (b < 64) begin
	       expected <= 6 + 3 * a;
	   end else if (b < 125) begin
	       expected <= 8 + 3 * a;
	   end else if (b < 216) begin
	       expected <= 10 + 3 * a;
	   end else if (b < 343) begin
	       expected <= 12 + 3 * a;
	   end 
	end
    
	if (!busy) begin
		$display("Input: a=%d, b=%d Result: y=%d Expected: y=%d", a, b, y_bo, expected);
		if (!rst) begin
			a <= a + 3;
			b <= b + 5;
			rst <= 1;
		end
	end
end

endmodule
    
    